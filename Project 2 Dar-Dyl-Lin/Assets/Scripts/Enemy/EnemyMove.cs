﻿using UnityEngine;
using System.Collections;

/**
    @author Darrick Hilburn
    @date 9/9/2016

    This script holds the code for making an enemy move.
    */
public class EnemyMove : MonoBehaviour
{
    // Movement is an enum that holds values for
    //    the different movement options available
    enum Movement
    {
        WAIT,           // Do nothing
        FORWARD,        // Move forward
        FLANK_LEFT,     // Move left, face forward
        LEFT,           // Turn and move left
        FLANK_RIGHT,    // Move right, face forward
        RIGHT,          // Turn and move right
        BACKSTEP,       // Move back, face forward
        RETREAT         // Turn and move back
    }
    
    [Tooltip("How far the enemy will move.")]
    public float moveDistance;
    [Tooltip("How long it takes the enemy to move. A higher speed means less time, and vice versa!")]
    public float moveSpeed;
    [Tooltip("How long the enemy will wait when its AI tells it to wait.")]
    public float waitTime;

    [Range(0, 5)]
    public int maxHorizontalMoves;
    int horizontalPos;
    [Range(0, 5)]
    public int maxVerticalMoves;
    int verticalPos;

   
    bool moving;

    Movement move;

    Vector3 newPos;

    // Have the enemy start by detemining whether they move or wait.
    void Start ()
    {
        horizontalPos = 0;
        verticalPos = 0;
        DetermineMove();
    }
	
	// When the enemy is not moving, determine if they move or wait.
	void Update ()
    {
        if (!moving)
        {
            DetermineMove();
        } 
	}

    /**
        DetermineMove randomly determines a boolean to make an enemy either move or wait.
        The option chosen is reflected in the Unity Debug log.
        */
    void DetermineMove()
    {
        
        bool moveCheck;
        int moveDetermine = Random.Range(0, 5);
        if (maxHorizontalMoves.Equals(0) && maxVerticalMoves.Equals(0))
        {
            StartCoroutine(Wait());
        }
        else
        {
            Debug.Log("Move determine value is " + moveDetermine.ToString());
            if (moveDetermine.Equals(0))
                moveCheck = false;
            else
                moveCheck = true;

            if (moveCheck)
                Debug.Log("Enemy is moving!");
            else
                Debug.Log("Enemy is waiting!");

            if (moveCheck) StartCoroutine(MoveCoroutine());
            else StartCoroutine(Wait());
        }
    }

    IEnumerator DebugMove()
    {
        moving = true;
        Debug.Log("Current position: " + transform.position.ToString());
        Vector3 newPos = new Vector3(transform.position.x, transform.position.y, transform.position.z + moveDistance);
        Debug.Log("New position: " + newPos.ToString());

        Debug.Log("Starting move!");
        while(Vector3.Distance(transform.position, newPos) > 0.1f)
        {
            transform.position = Vector3.Lerp(transform.position, newPos, moveSpeed * Time.deltaTime);
            yield return null;
        }
        Debug.Log("Move complete!");
        yield return new WaitForSeconds(1f);
        Debug.Log("Coroutine complete!");
        moving = false;
    }

    IEnumerator MoveCoroutine()
    {
        moving = true;
        Debug.Log("Current position: " + transform.position.ToString());
        newPos = GetNewPosition();
        Debug.Log("New position: " + newPos.ToString());

        Debug.Log("Starting move!");
        while (Vector3.Distance(transform.position, newPos) > 0.1f)
        {
            transform.position = Vector3.Lerp(transform.position, newPos, moveSpeed * Time.deltaTime);
            yield return null;
        }
        Debug.Log("Move complete!");

        switch (move)
        {
            case(Movement.FLANK_RIGHT):
            case(Movement.RIGHT):
                horizontalPos++;
                break;
            case(Movement.FLANK_LEFT):
            case(Movement.LEFT):
                horizontalPos--;
                break;
            case(Movement.BACKSTEP):
            case(Movement.RETREAT):
                verticalPos--;
                break;
            default:
                verticalPos++;
                break;
        }

        yield return new WaitForSeconds(1f);
        Debug.Log("Coroutine complete!");
        moving = false;
    }

    Vector3 GetNewPosition()
    {
        move = GetMoveTypeSmart();

        switch(move)
        {
            case (Movement.FLANK_LEFT):
            case (Movement.LEFT):
                return new Vector3(transform.position.x - moveDistance, transform.position.y, transform.position.z);
            case (Movement.FLANK_RIGHT):
            case (Movement.RIGHT):
                return new Vector3(transform.position.x + moveDistance, transform.position.y, transform.position.z);
            case (Movement.BACKSTEP):
            case (Movement.RETREAT):
                return new Vector3(transform.position.x, transform.position.y, transform.position.z - moveDistance);
            default:
                return new Vector3(transform.position.x, transform.position.y, transform.position.z + moveDistance);
        }
    }

    Movement GetMoveTypeSimple()
    {
        int movement = Random.Range(0, 7);
        switch (movement)
        {
            case(1):
            case(2):
                return Movement.FLANK_RIGHT;
            case(3):
            case(4):
                return Movement.FLANK_LEFT;
            case(5):
            case(6):
                return Movement.BACKSTEP;
            default:
                return Movement.FORWARD;
        }
    }

    Movement GetMoveTypeSmart()
    {
        int movement;

        #region Column Movement
        // Check if the enemy only moves in a column
        if (maxHorizontalMoves.Equals(0))
        {
            // First check if the enemy has moved out as far as they can
            if (Mathf.Abs(verticalPos).Equals(maxVerticalMoves))
            {
                // Check where the enemy is
                // positive = forward
                // negative = backwards

                // Positive: Enemy is as far forward as possible
                // They can only move backwards
                if (verticalPos > 0)
                {
                    movement = Random.Range(0, 2);
                    if (movement.Equals(0))
                        return Movement.BACKSTEP;
                    else
                        return Movement.RETREAT;
                }
                // Negative: Enemy is as far backwards as possible
                // They can only move forwards
                else
                {
                    return Movement.FORWARD;
                }
            }
            // If the enemy is not at an end, they can freely move up or down
            else
            {
                movement = Random.Range(0, 3);
                switch (movement)
                {
                    case(1):
                        return Movement.BACKSTEP;
                    case(2):
                        return Movement.RETREAT;
                    default:
                        return Movement.FORWARD;
                }
            }
        }
        #endregion

        #region Row Movement
        // Check if the enemy moves in a row
        else if (maxVerticalMoves.Equals(0))
        {
            // First check if the enemy has moved out as far as they can
            if (Mathf.Abs(horizontalPos).Equals(maxHorizontalMoves))
            {
                // Check where the enemy is
                // positive = right
                // negative = left

                // Positive: Enemy is as far right as possible
                // They can only move left
                if (horizontalPos > 0)
                {
                    movement = Random.Range(0, 2);
                    if (movement.Equals(0))
                        return Movement.FLANK_LEFT;
                    else
                        return Movement.LEFT;
                }
                // Negative: Enemy is as far left as possible
                // They can only move right
                else
                {
                    movement = Random.Range(0, 2);
                    if (movement.Equals(0))
                        return Movement.FLANK_RIGHT;
                    else
                        return Movement.RIGHT;
                }
            }
            // If the enemy is not at an end, they may move left or right
            else
            {
                movement = Random.Range(0, 4);
                switch (movement)
                {
                    case(0):
                        return Movement.FLANK_RIGHT;
                    case(1):
                        return Movement.RIGHT;
                    case(2):
                        return Movement.FLANK_LEFT;
                    case(3):
                        return Movement.LEFT;
                }
            }
        }
        #endregion

        #region Grid Movement
        // If enemy moves in neither a row or column, it has a grid to move in.
        else
        {
            // Check if enemy is in a corner
            if(Mathf.Abs(horizontalPos).Equals(maxHorizontalMoves) && Mathf.Abs(verticalPos).Equals(maxVerticalMoves))
            {
                // As far right and forward as possible
                // Enemy can move backwards or left
                if (horizontalPos.Equals(maxHorizontalMoves) && verticalPos.Equals(maxVerticalMoves))
                {
                    movement = Random.Range(0, 4);
                    switch (movement)
                    {
                        case(0):
                            return Movement.FLANK_LEFT;
                        case(1):
                            return Movement.LEFT;
                        case(2):
                            return Movement.BACKSTEP;
                        case(3):
                            return Movement.RETREAT;
                    }
                }
                // As far left and forward as possible
                // Enemy can move backwards or right
                else if (horizontalPos.Equals(maxHorizontalMoves * -1) && verticalPos.Equals(maxVerticalMoves))
                {
                    movement = Random.Range(0, 4);
                    switch (movement)
                    {
                        case(0):
                            return Movement.FLANK_RIGHT;
                        case(1):
                            return Movement.RIGHT;
                        case(2):
                            return Movement.BACKSTEP;
                        case(3):
                            return Movement.RETREAT;
                    }
                }
                // As far right and backwards as possible
                // Enemy can move forward or left
                else if (horizontalPos.Equals(maxHorizontalMoves) && verticalPos.Equals(maxVerticalMoves * -1))
                {
                    movement = Random.Range(0, 3);
                    switch (movement)
                    {
                        case(1):
                            return Movement.FLANK_LEFT;
                        case(2):
                            return Movement.RIGHT;
                        default:
                            return Movement.FORWARD;
                    }
                }
                // As far left and backwards as possible
                // Enemy can move forward or right
                else
                {
                    movement = Random.Range(0, 3);
                    switch (movement)
                    {
                        case(1):
                            return Movement.FLANK_RIGHT;
                        case(2):
                            return Movement.RIGHT;
                        default:
                            return Movement.FORWARD;
                    }
                }
            }
            // Check if enemy is on an edge
            else if (Mathf.Abs(horizontalPos).Equals(maxHorizontalMoves))
            {
                // Determine which edge enemy is on
                // Positive = right edge
                // Negative = left edge

                // Positive/Right edge
                // Enemy can move forward, backwards, or left
                if (horizontalPos > 0)
                {
                    movement = Random.Range(0, 5);
                    switch (movement)
                    {
                        case(1):
                            return Movement.FLANK_LEFT;
                        case(2):
                            return Movement.LEFT;
                        case(3):
                            return Movement.BACKSTEP;
                        case(4):
                            return Movement.RETREAT;
                        default:
                            return Movement.FORWARD;
                    }
                }
                // Negative/Left edge
                // Enemy can move forward, backwards, or right
                else
                {
                    movement = Random.Range(0, 5);
                    switch (movement)
                    {
                        case (1):
                            return Movement.FLANK_RIGHT;
                        case (2):
                            return Movement.RIGHT;
                        case (3):
                            return Movement.BACKSTEP;
                        case (4):
                            return Movement.RETREAT;
                        default:
                            return Movement.FORWARD;
                    }
                }
            }
            else if (Mathf.Abs(verticalPos).Equals(maxVerticalMoves))
            {
                // Determine which edge enemy is on
                // Positive = forward edge
                // Negative = backward edge

                // Positive/Forward edge
                // Enemy can move right, left, or backwards
                if (horizontalPos > 0)
                {
                    movement = Random.Range(0, 6);
                    switch (movement)
                    {
                        case (0):
                            return Movement.FLANK_RIGHT;
                        case (1):
                            return Movement.RIGHT;
                        case (2):
                            return Movement.FLANK_LEFT;
                        case (3):
                            return Movement.LEFT;
                        case (4):
                            return Movement.BACKSTEP;
                        case (5):
                            return Movement.RETREAT;
                        
                    }
                }
                // Negative/Back edge
                // Enemy can move right, left, or forward
                else
                {
                    movement = Random.Range(0, 5);
                    switch (movement)
                    {
                        case (1):
                            return Movement.FLANK_RIGHT;
                        case (2):
                            return Movement.RIGHT;
                        case (3):
                            return Movement.FLANK_LEFT;
                        case (4):
                            return Movement.LEFT;
                        default:
                            return Movement.FORWARD;
                    }
                }
            }
            // Enemy is not in a corner or edge; enemy can move freely
            else
            {
                movement = Random.Range(0, 7);
                switch (movement)
                {
                    case(1):
                        return Movement.FLANK_RIGHT;
                    case(2):
                        return Movement.RIGHT;
                    case(3):
                        return Movement.FLANK_LEFT;
                    case(4):
                        return Movement.LEFT;
                    case(5):
                        return Movement.BACKSTEP;
                    case(6):
                        return Movement.RETREAT;
                    default:
                        return Movement.FORWARD;
                }
            }
        }
        #endregion

        // This return only reached if somehow no other returns reached.
        return 0;
    }

    IEnumerator Wait()
    {
        moving = true;
        Debug.Log("Waiting . . . . . . ");
        yield return new WaitForSeconds(waitTime);
        Debug.Log("Wait complete!");
        yield return new WaitForSeconds(1f);
        moving = false;
    }
}
