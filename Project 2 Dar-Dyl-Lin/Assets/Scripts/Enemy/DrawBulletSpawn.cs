﻿using UnityEngine;
using System.Collections;

public class DrawBulletSpawn : MonoBehaviour
{
    void OnDrawGizmosSelected()
    {
        const float BARREL_SIZE = 0.1f;
        Gizmos.color = Color.red;
        Gizmos.DrawCube(transform.position, new Vector3(BARREL_SIZE, BARREL_SIZE, BARREL_SIZE));
    }
	
}
