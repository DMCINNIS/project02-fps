﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour
{

    GameObject Boss;
    public Transform enemy;
    public Vector3 axis = Vector3.up;
    public Vector3 desiredPosition;
    public float radius = 2.0f;
    public float radiusSpeed = 0.5f;
    public float rotationSpeed = 80.0f;
    public float moveSpeed = 5.0f;

    public float hoofDamage;

    Animator anim;
    


    void Start()
    {
        Boss = GameObject.FindWithTag("Enemy");
        enemy = Boss.transform;

        anim = GetComponent<Animator>();
    }

    void Update()
    {
        transform.LookAt(enemy);

        if (Input.GetKey(KeyCode.A) && !(Input.GetKey(KeyCode.D))
            && !(Input.GetKey(KeyCode.S)))
        {

            CircleLeft();
        }

        if (Input.GetKey(KeyCode.D) && !(Input.GetKey(KeyCode.A))
            && !(Input.GetKey(KeyCode.S)))
        {

            CircleRight();
        }

        if (Input.GetKey(KeyCode.W) && !(Input.GetKey(KeyCode.S))
            && !(Input.GetKey(KeyCode.A)) && !(Input.GetKey(KeyCode.D))) 
        {

            transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
         
        }

        //if input is S  then block, set defense to 9 AND if defense is 9 on collision move back a few steps
        if (Input.GetKey(KeyCode.S) && !(Input.GetKey(KeyCode.A))
        && !(Input.GetKey(KeyCode.D)))
        {
            //block            
        }

        if (Input.GetKey(KeyCode.Space) && !(Input.GetKey(KeyCode.A))
        && !(Input.GetKey(KeyCode.D)))
        {
            // if space bar jump forward a little and change state to charging.
            //On collision deal a lot of damage and reduce stamina by a decent amount. add ear to inventory

        }

        if(Input.GetKey(KeyCode.Mouse0) && !(Input.GetKey(KeyCode.Mouse1)) 
            && !(Input.GetKey(KeyCode.A)) && !(Input.GetKey(KeyCode.D)))
        {
            JabLeft();
            anim.SetBool("isJabLeft", false);
        }

        // on fire 2 punch right

        if (Input.GetKey(KeyCode.Mouse1) && !(Input.GetKey(KeyCode.Mouse0))
            && !(Input.GetKey(KeyCode.A)) && !(Input.GetKey(KeyCode.D)))
        {
            JabRight();
            anim.SetBool("isJabRight", false);
            
        }

        //on fire 1 and A strafe left hook
        if (Input.GetKey(KeyCode.Mouse0) && (Input.GetKey(KeyCode.A))
            && !(Input.GetKey(KeyCode.Mouse1)) && !(Input.GetKey(KeyCode.D)))
        {
            HookLeft();
            anim.SetBool("isHookLeft", false);
            hoofDamage /= 3.0f;
        }

        if (Input.GetKey(KeyCode.Mouse1) && (Input.GetKey(KeyCode.D))
            && !(Input.GetKey(KeyCode.Mouse0)) && !(Input.GetKey(KeyCode.A)))
        {
            HookRight();
            anim.SetBool("isHookRight", false);
            hoofDamage /= 3.0f;
        }
        
    }

    public void CircleLeft()
    {
        transform.RotateAround(enemy.position, axis, rotationSpeed * Time.deltaTime);
        desiredPosition = (transform.position - enemy.position).normalized * radius + enemy.position;
        transform.position = Vector3.MoveTowards(transform.position, desiredPosition, Time.deltaTime * radiusSpeed);

    }
    public void CircleRight()
    {
        transform.RotateAround(enemy.position, -axis, rotationSpeed * Time.deltaTime);
        desiredPosition = (transform.position - enemy.position).normalized * radius + enemy.position;
        transform.position = Vector3.MoveTowards(transform.position, desiredPosition, Time.deltaTime * radiusSpeed);
    }

    public void JabLeft()
    {
        anim.SetBool("isJabLeft", true);

    }

    public void JabRight()
    {
        anim.SetBool("isJabRight", true);
    }

    public void HookLeft()
    {
        hoofDamage *= 3.0f;
        anim.SetBool("isHookLeft", true);

    }


    public void HookRight()
    {
        hoofDamage *= 3.0f;
        anim.SetBool("isHookRight", true);

    }


}