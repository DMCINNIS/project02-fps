﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ReturnToPlayScene : MonoBehaviour {

    // Update is called once per frame
    void Update()
    {

        if (Input.anyKeyDown)
        {
            SceneManager.LoadScene("03_MainMenu");
        }

    }
}
