﻿// Bare Knuckle Hoof Brawl
// LevelManager --  One LevelManager script should be applied to each gameplay scene
// Used to manage information held in each level:  
//      player lives, player health, number of collectibles picked up
// 
// Tags needed: 
// 	1. Bosses to be mind-controlled should be tagged "Boss"
//  2. Player character should be tagged "Player"
//  3. Objects that are placeholders for the respawn points should  be tagged "Respawn"

//@ Author Linda Lane
//@ Class LevelManager
//@ Date 9/10/2015

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{

    // HUD variables
    private int playerLives;            // 3 lives (red heart)
    private int playerHealth;           // Health bar
    private int playerStamina;          // Stamina bar

    [HideInInspector]
    public string activeAttack;        // The active attack upon entering the level
    [HideInInspector]
    public string[] attacks = { "Hooves", "Barbed", "Blades" };           // all three attacks

    // Inspector variables
    [Tooltip("Time Limit for Round. Number must be in seconds.")]
    public float timeLeft = 180.0f;     // Timer in seconds for the round
    public Text timerText;
    public Text roundText;

    [Tooltip("Usual number is between 300-1000.")]
    public int pickupSpawnRate;

    [Tooltip("Level in game. Used for debug purposes.")]
    public int levelNumber;             // identify which level by number IN THE INSPECTOR

    [Tooltip("Boss flag. Used for debug purposes.")]
    public bool bossWin;
    public GameObject randomPickup;

    // Player and level variables
    private Vector3 respawnPoint;       // Location to respawn in level
    private GameObject player;          // identify player
    

    public AudioClip backgroundMusic;   // Set up background music

    // Use this for initialization
    void Start()
    {

        // Setup defaults
        playerLives = 3;
        playerHealth = 100;
        playerStamina = 100;
        bossWin = false;
        activeAttack = "Hooves";


        GameObject.Find("RoundText").GetComponent<Text> ().text = "Round: " + levelNumber.ToString();
        

        // Set up inventory starting values


        // Set up timer
        InvokeRepeating("CountdownTimer", 0.1f, 0.1f);


        // Keep track of the current scene
        Scene currentScene = SceneManager.GetActiveScene();

        Debug.Log("This scene is" + currentScene.name);

        // Play appropriate background music for each level
        if (SingletonAudioSource.Instance != null)
        {
            if (currentScene.name == "00_StartScene" || currentScene.name == "01_Splash_Scene" || currentScene.name == "02_Credits" ||
                currentScene.name == "03_MainMenu" || currentScene.name == "04_Instructions" || currentScene.name == "04_WinScene" ||
                currentScene.name == "10_Locker_Room_Hub")
            {
                backgroundMusic = Resources.Load<AudioClip>("Audio/Synth Bass 16 bars");
            }
            else if (currentScene.name == "10_Round_1_Scene")
            {
                backgroundMusic = Resources.Load<AudioClip>("Audio/345 Street Justice");
            }
            else if (currentScene.name == "10_Round_2_Scene")
            {
                backgroundMusic = Resources.Load<AudioClip>("Audio/Retinal Racers");
            }
            else if (currentScene.name == "10_Round_3_Scene")
            {
                backgroundMusic = Resources.Load<AudioClip>("Audio/taiko-drums");
            }

            SingletonAudioSource.Instance.PlayBackground(backgroundMusic);
        }


        // Update the HUD 
        GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, playerHealth, playerStamina, activeAttack);
        

        // Set up spawn point
        respawnPoint = GameObject.FindWithTag("Respawn").transform.position;

        // identify player
        player = GameObject.FindWithTag("Player");
    }

    void Update ()
    {
        
        // Make pickup items spawn at random times
        int randomNumber = (int)Random.value * pickupSpawnRate;

        switch (randomNumber)
        {
            case 5:
                randomPickup = GameObject.Find("Flowers");
                break;

            Vector3 position = new Vector3(Random.Range(-10.0f, 10.0f), 0, Random.Range(-10.0f, 10.0f));
            Instantiate(randomPickup, position, Quaternion.identity);
        }

    }


    public void LoseHealth(int amount)
    {
        // Decrease health by 1
        playerHealth = playerHealth - amount;

        // Update the HUD image
        GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, playerHealth, playerStamina, activeAttack);

        if (playerHealth <= 0)
        {
            // Wait a second to let player watch their health be zero and let it sink in that the player has died
            Invoke("Die", 1.0f);
          
        }
    }

 

    public void AddHealth(int amount)
    {
        // Check to see if player health is low
        if (playerHealth < 100)
        {
            // If player Health is down, add 1 hp
            playerHealth = playerHealth + amount;
            if (playerHealth > 100)
                playerHealth = 100;

            // Update the HUD image
            GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, playerHealth, playerStamina, activeAttack);

        }
    }

  

    public void UpdateBoss()
    {
        // Increase or decrease key drop count
        bossWin = true;

        // call WinLevel to check to see if we have won the level (keyDrops are 7)
        WinLevel();

        // Update the HUD
        GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, playerHealth, playerStamina, activeAttack);

    }

    private void WinLevel()             // Open access to boss door inside the Hub
    {
        {

            SingletonGameController.Instance.CompleteLevel((levelNumber - 1));
           
            // Play fireworks

        }
    }

    public void Die()               // Player has died...
    {
        // Decrement playerLives by 1
        playerLives--;

        // Check if player is out of lives
        if (playerLives <= 0)
        {
            // If yes, Load last save
            SceneManager.LoadScene("10_Locker_Room_Hub");
        }

        else // player has died but is not out of lives
        {
            // Teleport player to spawn point in level
            player.transform.position = respawnPoint;

            // Restore player to full health
            playerHealth = 100;
            playerStamina = 100;
            activeAttack = "Hooves";

            //reset stamina bars appropriate for each level


            // Update other inventory items that should be at start


            // Update HUD
            GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, playerHealth, playerStamina, activeAttack);

        }

    }

    // @ CountdownTimer puts a limit on the time allowed in the round
    // Display time in Mins:seconds:tenths

    void CountdownTimer()
    {
        timeLeft -= 0.1f;
        int timeTenthsSeconds = (int)((timeLeft * 10.0f) % 60.0f);
        int timeSeconds = (int)((timeLeft * 10.0f) / 10.0f) % 60;
        int timeMinutes = (int)((timeLeft * 10.0f) / 10.0f)/60;
        timerText.text =  timeMinutes + ":" + timeSeconds + ":" + timeTenthsSeconds;

        if (timeLeft <= 0)
        {
            CancelInvoke("CountdownTimer");

            //Add to this
           // GameObject.Find("Player").GetComponent<PlayerMove>().GameOver();
        }


    }


}
