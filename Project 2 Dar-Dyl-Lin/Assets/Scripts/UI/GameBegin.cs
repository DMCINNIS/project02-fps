﻿// Bare Knuckle Hoof Brawl - Team Dar-Dyl_Lin
// Project 2 for GAME 221
//@ Class GameBegin
//@ Author Linda Lane
//@ Date Sept. 9, 2016

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameBegin : MonoBehaviour {


    // Update is called once per frame
    void Update()
    {


        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        else if (Input.anyKeyDown)
        {
            SceneManager.LoadScene("03_MainMenu");
        }

    }
}
