﻿// Bare Knuckle Hoof Brawl

//@ Linda Lane
//@ Date 9/11/2015
//@ Class PickupRotate
// This script makes pickups rotate so they are easier to see

using UnityEngine;
using System.Collections;

public class PickupRotate : MonoBehaviour {

    public int pickupRotateSpeed;

	
	// Update is called once per frame
	void Update ()
    {
        transform.Rotate(0, pickupRotateSpeed * Time.deltaTime, 0, Space.World);
	}
}
