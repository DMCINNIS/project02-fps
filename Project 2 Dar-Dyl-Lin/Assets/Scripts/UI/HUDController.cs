﻿// Bare Knuckle Hoof Brawl
// LevelManager --  One LevelManager script should be applied to each gameplay scene
// Used to manage information held in each level:  
//      player lives, player health, number of collectibles picked up
// 
// Tags needed: 
// 	1. Bosses to be mind-controlled should be tagged "Boss"
//  2. Player character should be tagged "Player"
//  3. Objects that are placeholders for the respawn points should  be tagged "Respawn"

//@ Author Linda Lane
//@ Class LevelManager
//@ Date 9/11/2015

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;

public class HUDController : MonoBehaviour
{

    [Header("HUD information")]

    // Disable HUD for initial scenes
    [Tooltip("Enter the name of the panel in the Canvas HUD")]
    public Image attacksPanel;

    [Tooltip("Enter the name of the panel in the Canvas HUD")]
    public Image inventoryPanel;

    [Tooltip("Enter the name of the panel in the Canvas HUD")]
    public Image healthTimerPanel;


    public static HUDController Instance;
    //public static HUDController Instance {get; private set;}


    // Use this for initialization
    void Start()
    {
        Instance = this;

        DisableHUD();

    }


    // Disable HUD for initial scenes

    public void DisableHUD()
    {
        // Get the current scene
        Scene currentScene = SceneManager.GetActiveScene();
        Debug.Log(" Current scene is " + currentScene.name);


        // Disable all HUD images for all scenes that don't use it

        if (currentScene.name == "OO_StartScene" || currentScene.name == "01_Splash_Scene" ||
            currentScene.name == "02_Credits" || currentScene.name == "03_MainMenu" ||
            currentScene.name == "04_Instructions" || currentScene.name == "05_WinScene")
        {
            attacksPanel.enabled = false;
            inventoryPanel.enabled = false;
            healthTimerPanel.enabled = false;
     
        }
    }


    void Update()
    {


    }


    public void UpdateInventoryBar()
    {

    }



    public void UpdateHUD(int playerLives, int playerHealth, int playerStamina, string activeAttack)
    //public void UpdateHUD () 
    {
        GameObject.Find("LivesCount").GetComponent<Text>().text = playerLives.ToString();

        // set up check for one of three options for attacks


        // set up check for playerStamina bar
       
        
        // set up check for playerHealth bar
        switch (playerHealth)
        {
            case 3:
                break;
            case 2:
                break;
            case 1:
                break;
            case 0:
                break;
            default:
               // Debug.Log("Error in playerHealth value: " + playerHealth);
                break;
        }


    }

}
