using UnityEngine;
using System.Collections;

public class SingletonPlayerInformation : MonoBehaviour 
{

	public static SingletonPlayerInformation Instance { get; private set;}

	void Awake ()
	{
		if (Instance != null && Instance != this) {
			Destroy (gameObject);
		}

		DontDestroyOnLoad (gameObject);
	}
}
