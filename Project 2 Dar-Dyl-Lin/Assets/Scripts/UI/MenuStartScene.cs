// Bare Knuckle Hoof Brawl - Team Dar-Dyl_Lin
// Project 2 for GAME 221
//@ Class MenuStartScene
//@ Author Linda Lane
//@ Date Sept. 9, 2016
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuStartScene : MonoBehaviour {

    public AudioClip backgroundMusic;

	// Use this for initialization
	void Start () {

        SingletonAudioSource.Instance.PlayBackground(backgroundMusic);
        SceneManager.LoadScene("01_Splash_Scene");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
